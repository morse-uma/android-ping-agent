# Android Ping Agent

This application acts as a wrapper for the Ping application on Android devices and is intended for use as a traffic generator during network performance test. The application can be used directly from the user interface or remotely, making it ideal for automatic testing.

## Usage

The application can be used on the terminal with the provided user interface, or remotely through adb commands.

### Adb commands

The application can be used remotely sending intents to the service via the startservice command of adb:

`adb shell am startservice -n com.uma.ping/.PingService`

The application accepts the following intents (-a):

`com.uma.ping.START (Requires parameters)`

`com.uma.ping.STOP`

Parameters for configuring Ping are passed as a comma separated list of (key)=(value) pairs with no spaces, using the intent extra (-e) com.uma.ping.PARAMETERS.

All of the following parameters need to be specified:
- `target`: HostName or IP of the target machine.
- `ttl`: IP time to live.
- `interval`: ping interval.
- `size`: packet size

In some cases, it is necessary to specify the user of the command with --user 0, this flag does not seem to have any adverse effect, so it is recommended to use it always.

#### Examples

`adb shell am startservice -n com.uma.ping/.PingService -a com.uma.ping.START -e com.uma.ping.PARAMETERS \"target=127.0.0.1,ttl=128,interval=1.0,size=56\" --user 0`

`adb shell am startservice -n com.uma.ping/.PingService -a com.uma.ping.STOP`

### Device Interface

The application can be used through the device interface. The ping parameters are configured through the text fields and executed/stopped pressing the button Start/Stop. 

Once the ping agent is initiated, the Log will be updated during the execution. 

## Authors

* Alberto Salmeron Moreno
* Bruno Garcia Garcia
* Gonzalo Chica Morales

## License

Copyright 2020 MORSE Research Group - University of Malaga

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
